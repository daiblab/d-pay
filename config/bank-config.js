module.exports = {
    openPlatformTestNet: {
        url : "https://testapi.open-platform.or.kr",
        auth: {
            company: {
                client_id: 'l7xxcc169dcd8ba54234b2b0dfeb405f9142',
                client_secret: '4d22a882515047a1bb763c1f130dbff1',
                scope: 'oob',
                grant_type: 'client_credentials'
            },
            personal: {
                authorization: 'Bearer 42345875-adcc-43e7-9492-acd7cedcd956',
                userSeqNo:'1100034656',
                fintechUseNo:'199003528057724453920971'
            }
        },
        withdraw: {
            wd_pass_phrase: 'NONE',      //개발용은  NONE으로, 상용은 값 지정해야 (오픈플랫폼 포털의 "마이페이지 > 서비스 이용설정" 메뉴에서 설정)
            wd_print_content: '이체 금액',
            name_check_option: 'off'
        }
    }
};