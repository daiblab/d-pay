module.exports = {
    db: {
        mongodb_user: 'dev:daiblab123@',
        mongodb_url: 'localhost:27017',
        db_name: 'dpay'
    },
    mqtt: {
        url: "mqtt://localhost"                             //mqtt://35.189.153.47
    },
    mailer: {
        service: 'Gmail',
        host: 'localhost',
        port: '465',
        user: 'mailer@daiblab.com',
        password: 'daiblab123',
    },
};