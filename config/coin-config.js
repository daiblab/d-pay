module.exports = {
    testnet: {
        address: '0x12248c236e1d7E8Ec32640aFe5443E3390b21004',
        contract: '0xd81623dbaf0da6fd191d271edbf1a19ca9775529', //테스트넷 배포 후 필수로 변경
        privateKey: 'A5B7524DF46BAC27132A0DEE2B76A1FF698145760CD80A1451360491884786F6',
        url: {
            btc: "https://testnet.blockchain.info",
            etherscan: "http://api-ropsten.etherscan.io",
            withdraw_coin:"https://ropsten.infura.io/v3/d970bde89efd491896cbd52f5322587d"
        }
    }
};