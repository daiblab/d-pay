var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/bank/:amount', function(req, res, next) {
    res.render('bank/payment', { title: 'Express', amount: req.params.amount });
});

router.get('/pg/card/:amount', function(req, res, next) {
    res.render('pg/payment', { title: 'Express', amount: req.params.amount });
});

module.exports = router;
