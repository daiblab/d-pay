const express = require('express');
const router = express.Router();
const banksController = require('../backend/controller/banks');

//금융플랫폼 권한 획득
router.get('/authorization',(req,res,next) => {
    banksController.auth(req, res);
});

//계좌 실명 조회
router.post('/inquiry/real_name', (req,res,next) => {
    banksController.validate(req, res);
});

//계좌 입금 이체(이용기관)
router.get('/transfer/withdraw/company', (req,res,next) => {
    banksController.withdrawCompany(req, res);
});

//계좌 잔액 조회
router.get('/account/balance', (req,res,next) => {
    banksController.getBalance(req, res);
});

//계좌 거래 내역 조회
router.get('/account/transactions', (req,res,next) => {
    //banksController.validate(req, res);
    console.log('준비중');
});

//계좌 출금 이체(사용자)
router.post('/transfer/withdraw/user', (req,res,next) => {
    banksController.withdrawPersonal(req, res);
});

//계좌 출금 이체 결과(사용자)
router.post('/transfer/withdraw/user/result', (req,res,next) => {
    banksController.withdrawPersonalResult(req, res);
});

//callback 처리
router.get('/callback', (req,res,next) => {
    //banksController.validate(req, res);
    console.log(req,res);
});

module.exports = router;
