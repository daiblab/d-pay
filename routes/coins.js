const express = require('express');
const router = express.Router();
const coinsController = require('../backend/controller/coins');

//코인 출금
router.get('/withdraw',(req,res,next) => {
    coinsController.withdraw(req, res);
});

//지갑 등록
router.post('/register',(req,res,next) => {
    //coinsController.regist(req, res);
    console.log('준비중');
});

//잔액 조회
router.get('/balance',(req,res,next) => {
    //coinsController.balance(req, res);
    console.log('준비중');
});

module.exports = router;