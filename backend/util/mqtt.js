const config = require('../../config/config');
const coinConfig = require('../../config/coin-config');
const coinService = require('../service/coins');
const mqtt = require('mqtt');
const schedule = require('node-schedule');
const nodemailer = require('nodemailer');
const smtpPool = require('nodemailer-smtp-pool');
const request = require('request');

let ether_url = coinConfig.testnet.url.ether;
let endJobCount = 20;

function WithdrawJob(sendJson) {
    let rule = '*/1 * * * *';
    let jobCount = 0;
    let job = schedule.scheduleJob(rule, function (time) {
        console.log('withdraw answer time =>', time);

        checkWithdraw(sendJson)
            .then(count => {
                console.log('count=>', count);
                if (count == 1) {
                    sendJson['from'] = "withdrawSchduler";
                    sendJson['status'] = true;
                    console.log('Ready jsonData=>', sendJson);
                    subscribeCoinScanSchduler(sendJson);

                    console.log('end job !!');
                    job.cancel();
                } else {
                    console.log('in progress!');

                    if (jobCount == endJobCount) {
                        console.log('exception jobCount==>', jobCount)
                        job.cancel();
                    }
                    jobCount++;
                }
            });
    });
}

function checkWithdraw(data) {
    return new Promise((resolve, reject) => {

        let transaction = data.transaction;
        let url = ether_url + "/api" +
            "?module=proxy" +
            "&action=eth_getTransactionReceipt" +
            "&txhash=" + transaction +
            "&apikey=QGIFBDNP6SMZSW6WSUNB6BSRXB1UJ7M5EK";

        request.get(url, function (err, res, body) {
            if (err) {
                console.log('api err=>', err)
                // reject(err)
            }

            let jsonBody = JSON.parse(body);
            let result = jsonBody['result'];

            console.log('result=>', result);
            if (result != null) {
                console.log('result status => ', 'ok');
                resolve(1)
            }
        });
    });
}

function subscribeCoinScanSchduler(sendJson) {
    console.log('withdraw ok!');

    let subtractCoin = Number(sendJson.total_coin) - Number(sendJson.coin);
    let completedPrice = Number(sendJson.price);

    let totalCoinJson = {};
    totalCoinJson['total_coin'] = subtractCoin;

    coinService.updateTotalCoin(sendJson.coinId, totalCoinJson)
        .then(result => {
            console.log('finished substract coin ==>', result);

            let history = {
                "completedCoin": subtractCoin,
                "completedPrice": completedPrice,
                "status": sendJson.status
            }
            coinService.updateWithdrawHistory(sendJson.historyId, history)
                .then(result => {
                    console.log('update withdraw history status!');
                })
        }).catch((err) => {
        console.error('err=>', err)
    });
}

function publishEmail(data) {
    return new Promise((resolve, reject) => {
        let from = data.from;
        let to = data.to;
        let title = data.subject;
        let subject = title;
// let html = '<p>This is paragraph.</p>';
        let text = data.text;

        let mailOptions = {
            from,
            to,
            subject,
            // html,
            text,
        };

        const transporter = nodemailer.createTransport(smtpPool({
            service: config.mailer.service,
            host: config.mailer.host,
            port: config.mailer.port,
            auth: {
                user: config.mailer.user,
                pass: config.mailer.password,
            },
            tls: {
                rejectUnauthorize: false,
            },
            maxConnections: 5,
            maxMessages: 10,
        }));

        transporter.sendMail(mailOptions, (err, res) => {
            if (err) {
                console.log('failed... => ', err);
                reject(err)
            } else {
                console.log('succeed... => ', res);
                resolve(res)
            }

            transporter.close();
        });
    })
}

exports.WithdrawJob = WithdrawJob;
exports.publishEmail = publishEmail;

