const request = require('request');
const config = require('../../config/bank-config');

function authCompany() {
    return new Promise((resolve, reject) => {
        let tokenParam = {
            "client_id": config.openPlatformTestNet.auth.company.client_id,
            "client_secret": config.openPlatformTestNet.auth.company.client_secret,
            "scope": config.openPlatformTestNet.auth.company.scope,
            "grant_type": config.openPlatformTestNet.auth.company.grant_type
        };

        let tokenHeaders = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        let bank_url = config.openPlatformTestNet.url;
        let tokenUrl = bank_url + '/oauth/2.0/token';

        request({
            uri: tokenUrl,
            method: 'POST',
            form: tokenParam,
            headers: tokenHeaders
        }, function (err, response, result) {
            let tmpResult = JSON.parse(result);
            if (tmpResult.rsp_code == undefined) {
                resolve(tmpResult);
            } else {
                reject(tmpResult);
            }
        });
    });
}

function authPersonal() {
    //추후 구현 예정
}

function validate(header, param) {
    let bank_url = config.openPlatformTestNet.url;
    let validationUrl = bank_url + '/v1.0/inquiry/real_name';
    return new Promise((resolve, reject) => {
        request({
            uri: validationUrl,
            method: 'POST',
            form: JSON.stringify(param),
            headers: header
        }, function (err2, res2, result2) {
            let result = JSON.parse(result2);
            if (result.rsp_code == "A0000") {
                resolve(result);
            } else {
                reject(result);
            }
        });
    });
}

function withdrawCompany(header, param) {
    let bank_url = config.openPlatformTestNet.url;
    let withdrawUrl = bank_url + '/v1.0/transfer/deposit2';
    return new Promise((resolve, reject) => {
        request({
            uri: withdrawUrl,
            method: 'POST',
            form: JSON.stringify(param),
            headers: header
        }, function (err2, res2, result2) {
            let result = JSON.parse(result2);
            if (result.rsp_code == "A0000") {
                resolve(result);
            } else {
                reject(result);
            }
        });
    });
}

function personalAccountList(header, param) {
    return new Promise((resolve, reject) => {
        let bank_url = config.openPlatformTestNet.url;
        let accountUrl = bank_url + '/account/list';

        request({
            uri: accountUrl + param,
            method: 'GET',
            headers: header
        }, function (err, response, result) {
            let tmpResult = JSON.parse(result);
            if (tmpResult.rsp_code == "A0000") {
                resolve(tmpResult);
            } else {
                reject(tmpResult);
            }
        });
    });
}

function getBalance(header, param) {
    return new Promise((resolve, reject) => {
        let bank_url = config.openPlatformTestNet.url;
        let balanceUrl = bank_url + '/account/balance';

        request({
            uri: balanceUrl + param,
            method: 'GET',
            headers: header
        }, function (err, response, result) {
            let tmpResult = JSON.parse(result);
            if (tmpResult.rsp_code == "A0000") {
                resolve(tmpResult);
            } else {
                reject(tmpResult);
            }
        });
    });
}

function withdrawPersonal(header, param) {
    return new Promise((resolve, reject) => {
        let bank_url = config.openPlatformTestNet.url;
        let withdrawUrl = bank_url + '/transfer/withdraw';

        request({
            uri: withdrawUrl,
            method: 'POST',
            form: JSON.stringify(param),
            headers: header
        }, function (err, response, result) {
            let tmpResult = JSON.parse(result);
            if (tmpResult.rsp_code == "A0000") {
                resolve(tmpResult);
            } else {
                reject(tmpResult);
            }
        });
    });
}

function withdrawPersonalResult(header, param) {
    return new Promise((resolve, reject) => {
        let bank_url = config.openPlatformTestNet.url;
        let withdrawUrl = bank_url + '/transfer/result';

        request({
            uri: withdrawUrl,
            method: 'POST',
            form: JSON.stringify(param),
            headers: header
        }, function (err, response, result) {
            let tmpResult = JSON.parse(result);
            if (tmpResult.rsp_code == "A0000") {
                resolve(tmpResult);
            } else {
                reject(tmpResult);
            }
        });
    });
}

exports.authCompany = authCompany;
exports.validate = validate;
exports.withdrawCompany = withdrawCompany;
exports.personalAccountList = personalAccountList;
exports.getBalance = getBalance;
exports.withdrawPersonal = withdrawPersonal;
exports.withdrawPersonalResult = withdrawPersonalResult;
