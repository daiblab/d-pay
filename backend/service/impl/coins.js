var CoinHistorys = require('../../model/coinHistorys');

function getByCoinId(coinId) {
    return new Promise((resolve, reject) => {
        CoinHistorys.findOne(
            {"_id": coinId},
            function(err, coinHistory) {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                console.log('getByCoinId done: ' + coinHistory);
                resolve(coinHistory);
            }
        )
    });
}

function createWithdrawHistory(coinId, data) {
    return new Promise((resolve, reject) => {
        var coinHistorys = new CoinHistorys(data);
        coinHistorys.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createWithdrawHistory done: ' + result);
                resolve(result);
            }
        });
    });
}

function updateTotalCoin(coinId, data) {

}

function updateWithdrawHistory(coinId, data) {

}

exports.getByCoinId = getByCoinId;
exports.createWithdrawHistory = createWithdrawHistory;
exports.updateTotalCoin = updateTotalCoin;
exports.updateWithdrawHistory = updateWithdrawHistory;