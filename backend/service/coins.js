const db = require('../utils/db');
const ethereumWeb3 = require('../../util/ethereumWeb3');
const mqtt = require('../../util/mqtt');
const coinsServiceImpl = require('impl/coins');

function withdraw(coinId, param) {
    return new Promise((resolve, reject) => {
        ethereumWeb3.withDraw(data)
            .then((transaction) => {
                console.log('withDraw() =>', transaction);
                param['transaction'] = transaction;

                db.connectDB(country)
                    .then(() => {
                        //withdraw history 저장
                        coinsServiceImpl.createWithdrawHistory(coinId, param)
                            .then((history) => {
                                let historyId = history._id;
                                console.log('historyId->', historyId);
                                param['historyId'] = history._doc.historyId;

                                coinsServiceImpl.getByCoinId(coinId)
                                    .then((coin) => {
                                        param['total_coin'] = coin._doc.total_coin;
                                        mqtt.WithdrawJob(param);

                                        resolve(history);
                                    }).catch((err) => {
                                    reject(err)
                                });
                            }).catch((err) => {
                            reject(err)
                        });
                    });
            });
    });
}

function updateTotalCoin(coinId, data) {
    return new Promise((resolve, reject) => {
        coinsServiceImpl.updateTotalCoin(coinId, data)
            .then((result) => {
                resolve(result);
            }).catch((err) => {
                reject(err)
            });
    });
}

function updateWithdrawHistory(coinId, data) {
    return new Promise((resolve, reject) => {
        coinsServiceImpl.updateWithdrawHistory(coinId, data)
            .then((result) => {
                resolve(result);
            }).catch((err) => {
            reject(err)
        });
    });
}
exports.withdraw = withdraw;
exports.updateTotalCoin = updateTotalCoin;
exports.updateWithdrawHistory = updateWithdrawHistory;