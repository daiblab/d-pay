var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var bankHistorysSchema = require('impl/bankHistorys');

module.exports = mongoose.model('BankHistorys', bankHistorysSchema);
