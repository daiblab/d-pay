var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var coinHistorysSchema = require('impl/coinHistorys');

module.exports = mongoose.model('CoinHistorys', coinHistorysSchema);
