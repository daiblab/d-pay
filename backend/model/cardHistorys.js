var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var cardHistorysSchema = require('impl/cardHistorys');

module.exports = mongoose.model('CardHistorys', cardHistorysSchema);
