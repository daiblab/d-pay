var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var cardHistorysSchema = new Schema({
    card_no: String,                //카드번호
    price: Number,                  //결제 금액
    regDate: String                 //history 등록 시간
});


module.exports = cardHistorysSchema;
