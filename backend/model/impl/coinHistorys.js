var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var coinHistorysSchema = new Schema({
    coinId: String,
    type: String,                   // ether, btc
    price: Number,                   // 구입한 금액
    dpay: Number,
    status: Number,
    regDate: String,                // instance가 만들어 졌을때 시간
    completeDate: String,            // 거래완료 되었을때 시간
});


module.exports = coinHistorysSchema;
