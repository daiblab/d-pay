var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bankHistorysSchema = new Schema({
    type: String,                   //1: 출금이체(개인), 2: 입금이체(회사)
    fintech_use_num: String,        //개인인 경우 fintech No
    bank_tran_id: String,           //이체 transaction Id
    price: Number,                  //이체 금액
    regDate: String,                //history 등록 시간
    from_bank_name: String,         //이체하는 은행 이름
    from_account_num_masked: String,//이체하는 계좌 번호
    from_account_holder_name: String,//이체하는 계좌 이름
    to_bank_name: String,           //이체되는 은행 이름
    to_account_num_masked: String,  //이체되는 계좌 번호
    to_account_holder_name: String  //이체되는 계좌 이름
});


module.exports = bankHistorysSchema;
