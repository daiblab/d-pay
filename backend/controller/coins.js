const DpayResponse = require('../util/DpayResponse');
const coinsService = require('../service/coins');

function withdraw(req, res) {
    let dPayResponse = new DpayResponse();

    let param = {
        address: req.body.address,
        coin: req.body.coin,
        price: req.body.price
    }

    coinsService.withdraw(param)
        .then((result) => {
            dPayResponse.code = 200;
            dPayResponse.data = result;
            res.status(200).send(dPayResponse.create())
        }).catch((err) => {
            console.error('err=>', err)
            dPayResponse.code = 500;
            dPayResponse.message = err;
            res.status(500).send(dPayResponse.create())
        });

}

exports.withdraw = withdraw;