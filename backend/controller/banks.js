const util = require('../util/common');
const DpayResponse = require('../util/DpayResponse');
const banksService = require('../service/banks');
const config = require('../../config/bank-config');

function auth(req, res) {

    let dPayResponse = new DpayResponse();

    banksService.authCompany()
        .then((result) => {
            if(result.rsp_code == undefined) {
                dPayResponse.code = 200;
                dPayResponse.data = result;
                res.status(200).send(dPayResponse.create())
            } else {
                console.log('error :' , JSON.parse(result));
                dPayResponse.code = 500;
                dPayResponse.message = result;
                res.status(500).send(dPayResponse.create())
            }
        }).catch((err) => {
            console.error('err=>', err)
            dPayResponse.code = 500;
            dPayResponse.message = err;
            res.status(500).send(dPayResponse.create())
        });
}

function validate(req, res) {

    let validationParam = {
        "bank_code_std": req.body.bankAccountType,
        "account_num": req.body.bankAccount,
        "account_holder_info_type": " ",
        "account_holder_info": req.body.birthday,
        "tran_dtime": util.formatDate(new Date())
    };

    let dPayResponse = new DpayResponse();

    banksService.authCompany()
        .then((auth) => {

            let validationHeaders = {
                "Authorization" : "Bearer " + auth.access_token,
                'Content-Type': 'application/json'
            }

            banksService.validate(validationHeaders, validationParam)
                .then((result) => {
                    if(result.rsp_code == "A0000") {
                        dPayResponse.code = 200;
                        dPayResponse.data = result;
                        res.status(200).send(dPayResponse.create())
                    } else {
                        console.log('error :' , JSON.parse(result));
                        dPayResponse.code = 500;
                        dPayResponse.message = result;
                        res.status(500).send(dPayResponse.create())
                    }
                }).catch((err) => {
                    console.error('err=>', err)
                    dPayResponse.code = 500;
                    dPayResponse.message = err;
                    res.status(500).send(dPayResponse.create())
                });
        }).catch((err) => {
            console.error('err=>', err)
            dPayResponse.code = 500;
            dPayResponse.message = err;
            res.status(500).send(dPayResponse.create())
        });
}

function withdrawCompany(req, res) {

    let withdrawParam = {
        "wd_pass_phrase": config.openPlatformTestNet.withdraw.wd_pass_phrase,
        "wd_print_content": config.openPlatformTestNet.withdraw.wd_print_content,
        "name_check_option": config.openPlatformTestNet.withdraw.name_check_option,
        "req_cnt" : 1,
        "req_list": [{
            "tran_no": 1,
            "bank_code_std": req.body.bankAccountType,
            "account_num": req.body.bankAccount,
            "account_holder_name": req.body.userName,
            "print_content": '계좌이체',
            "tran_amt": req.body.price
        }],
        "tran_dtime": util.formatDate(new Date())
    };

    let dPayResponse = new DpayResponse();

    banksService.authCompany()
        .then((auth) => {

            let withdrawHeaders = {
                "Authorization" : "Bearer " + auth.access_token,
                'Content-Type': 'application/json'
            }

            banksService.withdrawCompany(withdrawHeaders, withdrawParam)
                .then((result) => {
                    if(result.rsp_code == "A0000") {
                        //To-do : bankHistory 저장 추가
                        dPayResponse.code = 200;
                        dPayResponse.data = result;
                        res.status(200).send(dPayResponse.create())
                    } else {
                        console.log('error :' , JSON.parse(result));
                        dPayResponse.code = 500;
                        dPayResponse.message = result;
                        res.status(500).send(dPayResponse.create())
                    }
                }).catch((err) => {
                    console.error('err=>', err)
                    dPayResponse.code = 500;
                    dPayResponse.message = err;
                    res.status(500).send(dPayResponse.create())
                });
        }).catch((err) => {
            console.error('err=>', err)
            dPayResponse.code = 500;
            dPayResponse.message = err;
            res.status(500).send(dPayResponse.create())
        });
}

function getBalance(req, res) {
    let dPayResponse = new DpayResponse();

    //쇼케이스에서는 개인 인증 token을 받았다고 가정하고 진행
    let accountParam = '?user_seq_no='+config.openPlatformTestNet.auth.personal.userSeqNo+'&include_cancel_yn=Y&sort_order=D';
    let accountHeaders = {
        "Authorization": config.openPlatformTestNet.auth.personal.authorization
    };

    banksService.personalAccountList(accountHeaders, accountParam)
        .then((accounts) => {
            if (accounts.rsp_code == "A0000") {
                let arr_balance = [];
                for(var i in accounts.res_list) {
                    let balanceParam = '?fintech_use_num=' + accounts.res_list[i].fintech_use_num + '&tran_dtime=' + util.formatDate(new Date());
                    banksService.getBalance(accountHeaders, balanceParam)
                        .then((result) => {
                            arr_balance.push(result);
                            accounts.res_list[i]['balance'] = result;
                            if(arr_balance.length == accounts.res_list.length) {
                                dPayResponse.code = 200;
                                dPayResponse.data = accounts;
                                res.status(200).send(dPayResponse.create())
                            }
                        });
                }
            } else {
                console.log('error :', JSON.parse(accounts));
                dPayResponse.code = 500;
                dPayResponse.message = accounts;
                res.status(500).send(dPayResponse.create())
            }
        }).catch((err) => {
            console.error('err=>', err)
            dPayResponse.code = 500;
            dPayResponse.message = err;
            res.status(500).send(dPayResponse.create())
        });;
}

function withdrawPersonal(req, res) {
    let withdrawParam = {
        "dps_print_content": req.body.dps_print_content,
        "fintech_use_num": req.body.fintech_use_num,
        "tran_amt": req.body.tran_amt,
        "tran_dtime": util.formatDate(new Date())
    };

    let withdrawHeaders = {
        "Authorization" : config.openPlatformTestNet.auth.personal.authorization,
        'Content-Type': 'application/json'
    };

    let dPayResponse = new DpayResponse();

    banksService.withdrawPersonal(withdrawHeaders, withdrawParam)
        .then((result) => {
            if(result.rsp_code == "A0000") {
                //To-do : bankHistory 저장 추가
                dPayResponse.code = 200;
                dPayResponse.data = result;
                res.status(200).send(dPayResponse.create())
            } else {
                console.log('error :' , JSON.parse(result));
                dPayResponse.code = 500;
                dPayResponse.message = result;
                res.status(500).send(dPayResponse.create())
            }
        }).catch((err) => {
            console.error('err=>', err)
            dPayResponse.code = 500;
            dPayResponse.message = err;
            res.status(500).send(dPayResponse.create())
        });
}

function withdrawPersonalResult(req, res) {
    let withdrawParam = {
        "check_type": "1",  // 1: withdraw(개인), 2: deposit(회사)
        "req_cnt": "1",
        "req_list": [
            {
                "tran_no": req.body.tran_no,
                "org_bank_tran_id": req.body.org_bank_tran_id,
                "org_bank_tran_date": req.body.org_bank_tran_date,
                "org_tran_amt": req.body.org_tran_amt
            }],
        "tran_dtime": util.formatDate(new Date())
    };

    let dPayResponse = new DpayResponse();

    banksService.authCompany()
        .then((auth) => {

            let withdrawHeaders = {
                "Authorization": "Bearer " + auth.access_token,
                'Content-Type': 'application/json'
            }

            banksService.withdrawPersonalResult(withdrawHeaders, withdrawParam)
                .then((result) => {
                    if (result.rsp_code == "A0000") {
                        dPayResponse.code = 200;
                        dPayResponse.data = result;
                        res.status(200).send(dPayResponse.create())
                    } else {
                        console.log('error :', JSON.parse(result));
                        dPayResponse.code = 500;
                        dPayResponse.message = result;
                        res.status(500).send(dPayResponse.create())
                    }
                }).catch((err) => {
                    console.error('err=>', err)
                    dPayResponse.code = 500;
                    dPayResponse.message = err;
                    res.status(500).send(dPayResponse.create())
                });
        }).catch((err) => {
            console.error('err=>', err)
            dPayResponse.code = 500;
            dPayResponse.message = err;
            res.status(500).send(dPayResponse.create())
        });
}


exports.auth = auth;
exports.validate = validate;
exports.withdrawCompany = withdrawCompany;
exports.getBalance = getBalance;
exports.withdrawPersonal = withdrawPersonal;
exports.withdrawPersonalResult = withdrawPersonalResult;